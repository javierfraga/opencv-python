import sys
import numpy as np
import cv2
from matplotlib import pyplot as plt

def main():
    """TODO: Docstring for main.
    :returns: TODO

    """
    if len(sys.argv) < 2:
        print( 'ERROR: file name and path not passed' )
        sys.exit(0)
    filein = sys.argv[1]
    # Load an color image in grayscale
    print( 'press escape to quit' )
    print( 'press s to save file' )
    print( 'press m to see what looks like in matplotlib' )
    img = cv2.imread(filein,0)
    cv2.imshow('image',img)

    k = cv2.waitKey(0)
    if k == 27:         # wait for ESC key to exit
        pass
    elif k == ord('s'): # wait for 's' key to save and exit
        if len(sys.argv) < 3:
            print( 'ERROR: output file name not given' )
            cv2.destroyAllWindows()
            sys.exit(0)
        fileout = sys.argv[2]
        cv2.imwrite(fileout,img)
    elif k == ord('m'):
        plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
        plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
        plt.show()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
