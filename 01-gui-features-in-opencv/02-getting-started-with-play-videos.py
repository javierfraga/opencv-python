import numpy as np
import cv2
import sys

def main():
    """TODO: Docstring for manin.
    :returns: TODO

    """
    if len(sys.argv) < 2:
        video = 0
        print( 'file not passed will use camera' )
    else:
        video = sys.argv[1]
    cap = cv2.VideoCapture( video )

    print( 'press q to quit' )
    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()

        # Our operations on the frame come here
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Display the resulting frame
        cv2.imshow('frame',gray)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
