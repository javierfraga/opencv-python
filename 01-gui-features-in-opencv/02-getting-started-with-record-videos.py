import numpy as np
import cv2
import sys

def main():
    """TODO: Docstring for manin.
    :returns: TODO

    """
    # if len(sys.argv) < 2:
        # video = 0
        # print( 'file not passed will use camera' )
    # else:
        # video = sys.argv[1]
    # cap = cv2.VideoCapture( video )

    # print( 'press q to quit' )
    cap = cv2.VideoCapture(0)
    # cap = cv2.VideoCapture(-1)

    # Define the codec and create VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter('output.avi',fourcc, 20.0, (640,480))

    while(cap.isOpened()):
        ret, frame = cap.read()
        if ret==True:
            # frame = cv2.flip(frame,0)

            # write the flipped frame
            out.write(frame)

            cv2.imshow('frame',frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            break

    # Release everything if job is finished
    cap.release()
    out.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
