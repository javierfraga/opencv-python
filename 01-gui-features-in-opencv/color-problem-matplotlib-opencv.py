# https://stackoverflow.com/questions/15072736/extracting-a-region-from-an-image-using-slicing-in-python-opencv/15074748#15074748
import sys
import cv2
import numpy as np
import timeit
import matplotlib.pyplot as plt
NUMPY_ONLY = True

def main():
    """TODO: Docstring for main.
    :returns: TODO

    """
    if len(sys.argv) < 2:
        print( 'ERROR: file name and path not passed' )
        sys.exit(0)
    filein = sys.argv[1]
    img = cv2.imread( filein )
    if NUMPY_ONLY:
        start = timeit.default_timer()
        img2 = img[:,:,::-1]
        stop = timeit.default_timer()
        print 'numpy only: ', stop - start
    else:
        pass
        start = timeit.default_timer()
        b,g,r = cv2.split(img)
        stop = timeit.default_timer()
        print 'cv2 split: ', stop - start
        start = timeit.default_timer()
        img2 = cv2.merge([r,g,b])
        stop = timeit.default_timer()
        print 'cv2 merge: ', stop - start

    plt.subplot(121).set_title('original');plt.imshow(img) # expects distorted color
    plt.subplot(122).set_title('reversed');plt.imshow(img2) # expect true color
    plt.suptitle("Matplotlib", size=16)
    plt.show()

    cv2.imshow('bgr image',img) # expects true color
    cv2.imshow('rgb image',img2) # expects distorted color
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
