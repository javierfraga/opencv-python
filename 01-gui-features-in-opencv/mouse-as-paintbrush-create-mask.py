import cv2
import numpy as np
import argparse
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='Merge set of files together')
parser.add_argument('-i','--image', type=str , required=True , metavar='' , help='Image to paint')
args = parser.parse_args()

drawing = False # true if mouse is pressed
ix,iy = -1,-1

# mouse callback function
def draw_circle(event,x,y,flags,param):
    # global ix,iy,drawing,mode
    ix,iy = -1,-1
    global drawing
    print('event:', event)
    print('x:', x)
    print('y:', y)
    print('flags:', flags)
    print('param:', param)
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix,iy = x,y

    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing == True:
            cv2.circle(img,(x,y),5,(0,0,255),-1)
            cv2.circle(mask,(x,y),5,(255,255,255),-1)

    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        cv2.circle(img,(x,y),5,(0,0,255),-1)
        cv2.circle(mask,(x,y),5,(255,255,255),-1)

img = cv2.imread( args.image )
mask = np.zeros_like( img )
cv2.namedWindow('image')
cv2.setMouseCallback('image',draw_circle, {'name':'javier','id':100})

print( 'Hit ESCAPE to quit' )
print( 'click and drag' )
print( 'press m to toggle brush' )
while(1):
    cv2.imshow('image',img)
    k = cv2.waitKey(1) & 0xFF
    if k == 27:
        break

cv2.destroyAllWindows()

plt.imshow( mask , cmap='gray' ); plt.show()
