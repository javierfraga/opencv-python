import sys
import numpy as np
import cv2
from matplotlib import pyplot as plt

if len(sys.argv) < 3:
    print( 'ERROR: needs to files passed' )
    sys.exit(0)
filein01 = sys.argv[1]
filein02 = sys.argv[2]

img1 = cv2.imread( filein01 )
img2 = cv2.imread( filein02 )

alpha = 0.5
dst = cv2.addWeighted(img1,alpha,img2,(1 - alpha),0)

cv2.imshow('dst',dst)
cv2.waitKey(0)
cv2.destroyAllWindows()
