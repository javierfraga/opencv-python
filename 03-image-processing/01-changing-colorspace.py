import sys
import cv2
import numpy as np

if len(sys.argv) < 2:
    print( 'ERROR: file name and path not passed' )
    sys.exit(0)
color = sys.argv[1]

# define range of color in HSV
if color == 'B' or color == 'b':
    lower = np.array([110,50,50])
    upper = np.array([130,255,255])
elif color == 'G' or color == 'g':
    lower = np.array([50,50,50])
    upper = np.array([70,255,255])
else:
    lower = np.array([-10,50,50])
    upper = np.array([20,255,255])

cap = cv2.VideoCapture(0)

while(1):

    # Take each frame
    _, frame = cap.read()

    # Convert BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # Threshold the HSV image to get only one color
    mask = cv2.inRange(hsv, lower, upper)

    # Bitwise-AND mask and original image
    res = cv2.bitwise_and(frame,frame, mask= mask)

    cv2.imshow('frame',frame)
    cv2.imshow('mask',mask)
    cv2.imshow('res',res)
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

cv2.destroyAllWindows()
