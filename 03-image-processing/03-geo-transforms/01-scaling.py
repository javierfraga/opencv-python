import sys
import cv2
import numpy as np

if len(sys.argv) < 4:
    print( 'ERROR: ./01-scaling.py <filein> <width> <height> <fileout, optional>' )
    sys.exit(0)
filein = sys.argv[1]
width = int(sys.argv[2])
height = int(sys.argv[3])

img = cv2.imread( filein )

# res = cv2.resize(img,None,fx=2, fy=2, interpolation = cv2.INTER_CUBIC)
#OR
# height, width = img.shape[:2]
# res = cv2.resize(img,(2*width, 2*height), interpolation = cv2.INTER_CUBIC)

res = cv2.resize(img,(width, height), interpolation = cv2.INTER_CUBIC)

cv2.imshow('res',res)
cv2.waitKey(0)
cv2.destroyAllWindows()

try:
    fileout = sys.argv[4]
    cv2.imwrite(fileout,res)
except Exception as e:
    pass
