import sys
import cv2
import numpy as np

if len(sys.argv) < 4:
    print( 'ERROR: ./02-translation.py <filein> <x-shift> <y-shift> <fileout, optional>' )
    sys.exit(0)
filein = sys.argv[1]
x_shift = int(sys.argv[2])
y_shift = int(sys.argv[3])

img = cv2.imread( filein ,0)
rows,cols = img.shape

M = np.float32([[1,0,x_shift],[0,1,y_shift]])
dst = cv2.warpAffine(img,M,(cols,rows))

cv2.imshow('img',dst)
cv2.waitKey(0)
cv2.destroyAllWindows()

try:
    fileout = sys.argv[4]
    cv2.imwrite(fileout,res)
except Exception as e:
    pass
