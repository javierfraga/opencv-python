import sys
import cv2
import numpy as np

if len(sys.argv) < 3:
    print( 'ERROR: ./03-rotation.py <filein> <angle_shift> <fileout, optional>' )
    sys.exit(0)
filein = sys.argv[1]
angle_shift = int(sys.argv[2])

img = cv2.imread( filein ,0)
rows,cols = img.shape

M = cv2.getRotationMatrix2D((cols/2,rows/2),angle_shift,1)
dst = cv2.warpAffine(img,M,(cols,rows))

cv2.imshow('img',dst)
cv2.waitKey(0)
cv2.destroyAllWindows()

try:
    fileout = sys.argv[4]
    cv2.imwrite(fileout,res)
except Exception as e:
    pass
