import sys
import cv2
import numpy as np
from matplotlib import pyplot as plt

if len(sys.argv) < 2:
    print( 'ERROR: ./04-affine-transformation.py <filein> <fileout, optional>' )
    sys.exit(0)
filein = sys.argv[1]

img = cv2.imread( filein)
rows,cols,ch = img.shape

pts1 = np.float32([[50,50],[200,50],[50,200]])
pts2 = np.float32([[10,100],[200,50],[100,250]])

M = cv2.getAffineTransform(pts1,pts2)

dst = cv2.warpAffine(img,M,(cols,rows))

plt.subplot(121),plt.imshow(img),plt.title('Input')
plt.subplot(122),plt.imshow(dst),plt.title('Output')
plt.show()

try:
    fileout = sys.argv[2]
    cv2.imwrite(fileout,dst)
except Exception as e:
    pass
