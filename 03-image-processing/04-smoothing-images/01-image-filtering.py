import sys
import cv2
import numpy as np
from matplotlib import pyplot as plt

if len(sys.argv) < 2:
    print( 'ERROR: ./01-image-filtering.py <filein> <fileout, optional>' )
    sys.exit(0)
filein = sys.argv[1]

img = cv2.imread( filein )

kernel = np.ones((5,5),np.float32)/25
dst = cv2.filter2D(img,-1,kernel)

plt.subplot(121),plt.imshow(img),plt.title('Original')
plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(dst),plt.title('Averaging')
plt.xticks([]), plt.yticks([])
plt.show()

try:
    fileout = sys.argv[2]
    cv2.imwrite(fileout,dst)
except Exception as e:
    pass
