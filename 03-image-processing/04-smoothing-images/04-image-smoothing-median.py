import sys
import cv2
import numpy as np
from matplotlib import pyplot as plt

if len(sys.argv) < 2:
    print( 'ERROR: ./01-image-filtering.py <filein> <fileout, optional>' )
    sys.exit(0)
filein = sys.argv[1]

img = cv2.imread( filein )

blur = cv2.medianBlur(img,5)

plt.subplot(121),plt.imshow(img),plt.title('Original')
plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(blur),plt.title('Blurred')
plt.xticks([]), plt.yticks([])
plt.show()

try:
    fileout = sys.argv[2]
    cv2.imwrite(fileout,dst)
except Exception as e:
    pass
