import sys
import cv2
import numpy as np
from matplotlib import pyplot as plt

if len(sys.argv) < 2:
    print( 'ERROR: ./01-erosion.py <filein> <fileout, optional>' )
    sys.exit(0)
filein = sys.argv[1]

img = cv2.imread( filein ,0)
kernel = np.ones((5,5),np.uint8)
output = cv2.morphologyEx(img, cv2.MORPH_TOPHAT, kernel)

plt.subplot(121),plt.imshow(img, cmap = 'gray', interpolation = 'bicubic'),plt.title('Original')
plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(output, cmap = 'gray', interpolation = 'bicubic'),plt.title('transformed')
plt.xticks([]), plt.yticks([])
plt.show()

try:
    fileout = sys.argv[2]
    cv2.imwrite(fileout,output )
except Exception as e:
    pass
