import sys
import cv2
import numpy as np
from matplotlib import pyplot as plt

if len(sys.argv) < 2:
    print( 'ERROR: ./01-erosion.py <filein>' )
    sys.exit(0)
filein = sys.argv[1]

img = cv2.imread( filein ,0)
edges = cv2.Canny(img,100,200)

plt.subplot(121),plt.imshow(img,cmap = 'gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(edges,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])

plt.show()
