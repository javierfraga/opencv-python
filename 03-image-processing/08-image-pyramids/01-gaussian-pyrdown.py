import sys
import numpy as np
import cv2
from matplotlib import pyplot as plt

if len(sys.argv) < 2:
    print( 'ERROR: needs to files passed' )
    sys.exit(0)
filein = sys.argv[1]

img = cv2.imread( filein )
dst = cv2.pyrDown(img)

cv2.imshow('dst',dst)
cv2.waitKey(0)
cv2.destroyAllWindows()
