import cv2
import numpy as np,sys
from matplotlib import pyplot as plt

if len(sys.argv) < 3:
    print( 'ERROR: ./03-blending.py <filein01> <filein02> <fileout01, optional> <fileout02, optional>' )
    sys.exit(0)
filein01 = sys.argv[1]
filein02 = sys.argv[2]

A = cv2.imread( filein01 )
A = A[:,:,::-1]
B = cv2.imread( filein02 )
B = B[:,:,::-1]

# generate Gaussian pyramid for A
G = A.copy()
gpA = [G]
for i in xrange(6):
    G = cv2.pyrDown(G)
    gpA.append(G)

# generate Gaussian pyramid for B
G = B.copy()
gpB = [G]
for i in xrange(6):
    G = cv2.pyrDown(G)
    gpB.append(G)

# generate Laplacian Pyramid for A
lpA = [gpA[5]]
for i in xrange(5,0,-1):
    size = (gpA[i-1].shape[1], gpA[i-1].shape[0])
    GE = cv2.pyrUp(gpA[i], dstsize = size)
    L = cv2.subtract(gpA[i-1],GE)
    lpA.append(L)

# generate Laplacian Pyramid for B
lpB = [gpB[5]]
for i in xrange(5,0,-1):
    size = (gpB[i-1].shape[1], gpB[i-1].shape[0])
    GE = cv2.pyrUp(gpB[i], dstsize = size)
    L = cv2.subtract(gpB[i-1],GE)
    lpB.append(L)

# Now add left and right halves of images in each level
LS = []
for la,lb in zip(lpA,lpB):
    rows,cols,dpt = la.shape
    ls = np.hstack((la[:,0:cols/2], lb[:,cols/2:]))
    LS.append(ls)

# now reconstruct
ls_ = LS[0]
for i in xrange(1,6):
    size = (LS[i].shape[1], LS[i].shape[0])
    ls_ = cv2.pyrUp(ls_, dstsize = size)
    ls_ = cv2.add(ls_, LS[i])

# image with direct connecting each half
real = np.hstack((A[:,:cols/2],B[:,cols/2:]))

plt.subplot(121),plt.imshow(real),plt.title('Original')
plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(ls_),plt.title('transformed')
plt.xticks([]), plt.yticks([])
plt.show()

try:
    fileout01 = sys.argv[3]
    fileout02 = sys.argv[4]
    cv2.imwrite(fileout01,real[:,:,::-1] )
    cv2.imwrite(fileout02,ls_[:,:,::-1] )
except Exception as e:
    pass
