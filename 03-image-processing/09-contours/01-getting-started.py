import numpy as np
import cv2
import argparse
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='Get started learning contours')
parser.add_argument('-i','--input', type=str , metavar='' , required=True , help='Input file')
parser.add_argument('-c','--contour', type=int , help='Which contour do you want to plot')
args = parser.parse_args()

def main():
    """ Main
    :returns: None
    """
    im = cv2.imread( args.input  )
    imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
    ret,thresh = cv2.threshold(imgray,127,255,0)
    contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    if args.contour is None:
        img = cv2.drawContours(im, contours, -1, (0,255,0), 3)
    else:
        cnt = contours[ args.contour ]
        img = cv2.drawContours(im, [cnt], 0, (0,255,0), 3)
    plt.imshow( img )
    plt.show()

if __name__ == "__main__":
    main()
