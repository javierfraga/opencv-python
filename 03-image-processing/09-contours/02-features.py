import numpy as np
import cv2
import argparse
import matplotlib.pyplot as plt
import pprint

parser = argparse.ArgumentParser(description='Get started learning contours')
parser.add_argument('-i','--input', type=str , metavar='' , required=True , help='Input file')
parser.add_argument('-c','--contour', type=int , default=0 , help='Which contour do you want to plot')
parser.add_argument('-e','--epsilon', type=float , default=0.1 , help='Which epsilon % do you want')
parser.add_argument('-a','--approx', type=int , default=2 , help='Which approximation of findContours do you want')
args = parser.parse_args()

def main():
    """ Main
    :returns: None
    """
    img = cv2.imread( args.input  )
    imgray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    ret,thresh = cv2.threshold(imgray,127,255,0)
    contours,hierarchy = cv2.findContours(thresh, 1, args.approx)

    cnt = contours[ args.contour ]
    M = cv2.moments(cnt)
    pprint.pprint( M )

    print
    print 'Shape'
    print img.shape

    print
    print 'Centroid'
    cx = int(M['m10']/M['m00'])
    cy = int(M['m01']/M['m00'])
    print 'cx:', cx , ' cy:', cy

    print
    print 'Perimeter'
    perimeter = cv2.arcLength(cnt,True)
    print perimeter

    # Epsilon
    # TODO: is not working fix this
    # contours,hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    # cnt = contours[ args.contour ]
    epsilon = ( args.epsilon )*cv2.arcLength(cnt,True)
    approx = cv2.approxPolyDP(cnt,epsilon,True)
    img3 = cv2.drawContours(img, approx, -1, (0,255,0), 3)
    plt.imshow( img3 )
    plt.title( 'Epsilon' )
    plt.show()

    # Convex
    # TODO: is not working fix this
    k = cv2.isContourConvex(cnt)
    print k
    hull = cv2.convexHull(cnt)
    img3 = cv2.drawContours(img, hull, -1, (0,255,0), 3)
    plt.imshow( img3 )
    plt.title( 'Convex' )
    plt.show()

    x,y,w,h = cv2.boundingRect(cnt)
    cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
    plt.imshow( img )
    plt.title( 'Straight Bounding Rectangle' )
    plt.show()

    rect = cv2.minAreaRect(cnt)
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    cv2.drawContours(img,[box],0,(0,0,255),2)
    plt.imshow( img )
    plt.title( 'Rotated Rectangle' )
    plt.show()

    (x,y),radius = cv2.minEnclosingCircle(cnt)
    center = (int(x),int(y))
    radius = int(radius)
    cv2.circle(img,center,radius,(0,255,0),2)
    plt.imshow( img )
    plt.title( 'Minimum Enclosing Circle' )
    plt.show()

    ellipse = cv2.fitEllipse(cnt)
    img = cv2.ellipse(img,ellipse,(0,255,0),2)
    plt.imshow( img )
    plt.title( 'Fitting an Ellipse' )
    plt.show()

    rows,cols = img.shape[:2]
    [vx,vy,x,y] = cv2.fitLine(cnt, cv2.DIST_L2,0,0.01,0.01)
    lefty = int((-x*vy/vx) + y)
    righty = int(((cols-x)*vy/vx)+y)
    img = cv2.line(img,(cols-1,righty),(0,lefty),(0,255,0),2)
    plt.imshow( img )
    plt.title( 'Fitting a Line' )
    plt.show()

if __name__ == "__main__":
    main()
