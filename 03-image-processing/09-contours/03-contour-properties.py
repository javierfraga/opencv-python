import numpy as np
import cv2
import argparse
import matplotlib.pyplot as plt
import pprint

parser = argparse.ArgumentParser(description='Get started learning contours')
parser.add_argument('-i','--input', type=str , metavar='' , required=True , help='Input file')
parser.add_argument('-a','--approx', type=int , default=2 , help='Which approximation of findContours do you want')
parser.add_argument('-c','--contour', type=int , default=0 , help='Which contour do you want to plot')
args = parser.parse_args()

def main():
    """ Get extreme points of image
    :returns: None
    """
    img = cv2.imread( args.input  )
    imgray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    gray = cv2.bilateralFilter(imgray, 11, 17, 17)
    edged = cv2.Canny(gray, 30, 200)
    # ret,thresh = cv2.threshold(imgray,127,255,0)
    # ret,thresh = cv2.threshold(imgray,0,255,cv2.THRESH_OTSU)
    # contours,hierarchy = cv2.findContours(thresh, 1, args.approx)
    contours,hierarchy = cv2.findContours(edged, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    cnt = contours[ args.contour ]
    cnts = []
    for cnt in contours:
        for ct in cnt:
            coord = tuple(ct[0])
            # cv2.circle( img , coord , 7 , (255,0,0) , -1 )
            cnts.append( coord )
    cnts = np.array( cnts )
    __import__('ipdb').set_trace()
    index = cnts[:,0].argmin()
    topleft = cnts[ index ]
    index = cnts[:,0].argmax()
    bottomright = cnts[ index ]
    cv2.circle(img, tuple(topleft),  10, (255, 0, 0), -1)
    cv2.circle(img, tuple(bottomright),  10, (255, 0, 0), -1)
    # leftmost = tuple(cnt[cnt[:,:,0].argmin()][0])
    # rightmost = tuple(cnt[cnt[:,:,0].argmax()][0])
    # topmost = tuple(cnt[cnt[:,:,1].argmin()][0])
    # bottommost = tuple(cnt[cnt[:,:,1].argmax()][0])

    #  TODO: Not working fix #
    # cv2.circle(img, leftmost,  10, (0, 0, 255), -1)
    # cv2.circle(img, rightmost,  10, (0, 0, 255), -1)
    # cv2.circle(img, topmost,  10, (0, 0, 255), -1)
    # cv2.circle(img, bottommost,  10, (0, 0, 255), -1)
    # __import__('ipdb').set_trace()
    # cv2.drawContours(img, leftmost, -1, (0,255,0), 3)
    # cv2.drawContours(img, rightmost, -1, (0,255,0), 3)
    # cv2.drawContours(img, topmost, -1, (0,255,0), 3)
    # cv2.drawContours(img, bottommost, -1, (0,255,0), 3)
    plt.imshow( img )
    plt.title( 'Fitting a Line' )
    plt.show()

if __name__ == "__main__":
    main()
