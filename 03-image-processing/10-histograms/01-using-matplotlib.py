import argparse
import cv2
import numpy as np
from matplotlib import pyplot as plt

parser = argparse.ArgumentParser(description='Histograms')
parser.add_argument('-i','--input', type=str , metavar='' , required=True , help='Input file')
parser.add_argument('-m','--mask', action='store_true', help='Appy mask example')
args = parser.parse_args()

def main():
    """ Main
    :returns: TODO
    """
    img = cv2.imread( args.input  )
    print ( img.shape )
    print ( img.shape[1] * img.shape[1] )
    fig = plt.figure()

    plt.subplot(3, 1, 1)
    plt.imshow( img[...,::-1] )

    plt.subplot(3, 1, 2)
    img_ravel = img.ravel()
    print( img_ravel.shape )
    plt.hist(img.ravel(),256,[0,256]);

    plt.subplot(3, 1, 3)
    color = ('b','g','r')
    for i,col in enumerate(color):
        histr = cv2.calcHist([img],[i],None,[256],[0,256])
        plt.plot(histr,color = col)
        plt.xlim([0,256])
    plt.show()

    if args.mask:
        mask = np.zeros(img.shape[:2], np.uint8)
        mask[100:300, 100:400] = 255
        masked_img = cv2.bitwise_and(img,img,mask = mask)

        # Calculate histogram with mask and without mask
        # Check third argument for mask
        hist_full = cv2.calcHist([img],[0],None,[256],[0,256])
        hist_mask = cv2.calcHist([img],[0],mask,[256],[0,256])

        plt.subplot(221), plt.imshow(img[...,::-1], 'gray')
        plt.subplot(222), plt.imshow(mask,'gray')
        plt.subplot(223), plt.imshow(masked_img[...,::-1], 'gray')
        plt.subplot(224), plt.plot(hist_full), plt.plot(hist_mask)
        plt.xlim([0,256])
        plt.show()

if __name__ == "__main__":
    main()
